package org.javacs.kt.symbols

import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiWhiteSpace
import org.eclipse.lsp4j.DocumentSymbol
import org.eclipse.lsp4j.SymbolInformation
import org.eclipse.lsp4j.SymbolKind
import org.eclipse.lsp4j.jsonrpc.messages.Either
import org.javacs.kt.ClientConfiguration
import org.javacs.kt.position.range
import org.jetbrains.kotlin.psi.KtFile
import org.jetbrains.kotlin.psi.KtPackageDirective

typealias DocumentSymbolsList = List<Either<SymbolInformation, DocumentSymbol>>

fun documentSymbols(config: ClientConfiguration, file: KtFile): DocumentSymbolsList {
    fun doDocumentSymbols(element: PsiElement): List<DocumentSymbol> {
        fun getSelectionRange(element: PsiElement): TextRange {
            val children = element.children
            val hasChild = children.isNotEmpty()
            return if (element is KtPackageDirective && hasChild) children[0].textRange else element.textRange
        }

        val children = element.children.flatMap(::doDocumentSymbols)
        return element.pickHierarchyElements(config.kindsSupported)?.let { info ->
            val text = element.containingFile.text
            val symbolRange = range(text, element.textRange)
            val selectionRange = range(text, getSelectionRange(element))
            val symbol = DocumentSymbol(info.name, info.kind, symbolRange, selectionRange, info.detail, children)
            listOf(symbol)
        } ?: children
    }

    return doDocumentSymbols(file).map { Either.forRight(it) }
}

internal fun PsiElement.pickHierarchyElements(kindsSupported: List<SymbolKind>): ElementInfo? {
    val className = this::class.java.simpleName
    val result = if (this is PsiWhiteSpace) null else getElementInfo(this, className)
    return if (result != null && kindsSupported.contains(result.kind)) result else null
}
