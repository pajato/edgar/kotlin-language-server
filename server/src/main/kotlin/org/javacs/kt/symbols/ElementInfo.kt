package org.javacs.kt.symbols

import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import org.eclipse.lsp4j.SymbolKind
import org.jetbrains.kotlin.psi.*

data class ElementInfo(val name: String, val kind: SymbolKind, val detail: String)

internal fun getElementInfo(element: PsiElement, className: String): ElementInfo? {
    tailrec fun getAncestorName(element: PsiElement?): String = when (element) {
        null -> ""
        is KtNameReferenceExpression -> element.text
        is KtPackageDirective -> element.name
        is KtDotQualifiedExpression -> element.text
        is KtNamedDeclaration -> element.nameAsSafeName.asString()
        is KtDeclarationModifierList -> element.text
        is KtFile -> element.name
        else -> getAncestorName(element.parent)
    }

    tailrec fun getAncestorKind(element: PsiElement?): SymbolKind? = when (element) {
        null -> null
        is KtPackageDirective -> SymbolKind.Package
        is KtProperty -> SymbolKind.Property
        is KtPrimaryConstructor -> SymbolKind.Constructor
        is KtSecondaryConstructor -> SymbolKind.Constructor
        is KtFunction -> SymbolKind.Function
        is KtClass -> SymbolKind.Class
        is KtFile -> SymbolKind.File
        else -> getAncestorKind(element.parent)
    }

    val name = getAncestorName(element)
    val kind = getAncestorKind(element)
    return if (kind == null) null else ElementInfo(name, kind, className)
}
